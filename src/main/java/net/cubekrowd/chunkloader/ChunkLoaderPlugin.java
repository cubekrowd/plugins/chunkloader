/*

    cubekrowd/chunkloader
    Copyright (C) 2018  Foorack

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package net.cubekrowd.chunkloader;

import java.util.*;
import org.bukkit.*;
import org.bukkit.entity.*;
import org.bukkit.plugin.*;
import org.bukkit.plugin.java.*;
import org.bukkit.event.*;
import org.bukkit.event.player.*;
import org.bukkit.scheduler.*;
import org.spigotmc.event.player.*;

public class ChunkLoaderPlugin extends JavaPlugin implements Listener {

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
    }

    @Override
    public void onDisable() {
    }

    @EventHandler
    public void onPlayerSpawnLocation(PlayerSpawnLocationEvent e) {
	Player p = e.getPlayer();
	loadChunkRadius(p, 10, 10);
	loadChunkRadius(p, 20, 15);
    }

    public void loadChunkRadius(Player p, int max, int time) {
        new BukkitRunnable() {
            @Override
            public void run() {
		if(!p.isOnline()){
			return;
		}
		p.setViewDistance(max);
            }
        }.runTaskLater(this, time);
    }
}
